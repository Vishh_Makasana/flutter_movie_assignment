import 'package:flutter/material.dart';

class AppTheme {
  AppTheme._();

  static final ThemeData lightTheme = ThemeData(
    scaffoldBackgroundColor: Colors.white,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    hintColor: Color(0x80000000),
    cursorColor: Color(0x80000000),
    appBarTheme: AppBarTheme(
      color: Colors.white,
      iconTheme: IconThemeData(
        color: Colors.black87,
      ),
    ),
    colorScheme: ColorScheme.light(
      primary: Colors.white,
      onPrimary: Colors.white,
      primaryVariant: Colors.white38,
      secondary: Color(0xFF3B424E),
    ),
    cardTheme: CardTheme(
      color: Color(0x40000000),
    ),
    iconTheme: IconThemeData(
      color: Colors.black87,
    ),
    textTheme: TextTheme(
      headline6: TextStyle(
        color: Colors.black,
        fontWeight: FontWeight.bold,
        fontFamily: 'Rubik',
        fontSize: 14,
      ),
      headline3: TextStyle(
        color: Colors.black,
        fontWeight: FontWeight.bold,
        fontFamily: 'Rubik',
        fontSize: 30,
      ),
      headline5: TextStyle(
        color: Color(0xFFFFFFFF),
        letterSpacing: 1.5,
        fontSize: 18.0,
        fontWeight: FontWeight.bold,
        fontFamily: 'Rubik',
      ),
      subtitle2: TextStyle(
        color: Color(0xAAFF1744),
        fontSize: 12.0,
        fontFamily: 'Rubik',
      ),
      subtitle1: TextStyle(
        color: Color(0xFF3B424E),
        fontSize: 18.0,
        fontFamily: 'Rubik',
      ),
    ),
  );

  static final ThemeData darkTheme = ThemeData(
    scaffoldBackgroundColor: Color(0xFF3B424E),
    visualDensity: VisualDensity.adaptivePlatformDensity,
    hintColor: Color(0x80FFFFFF),
    cursorColor: Color(0x80FFFFFF),
    appBarTheme: AppBarTheme(
      color: Colors.black,
      iconTheme: IconThemeData(
        color: Colors.white,
      ),
    ),
    colorScheme: ColorScheme.light(
      primary: Colors.black,
      onPrimary: Colors.black,
      primaryVariant: Colors.black,
      secondary: Colors.white,
    ),
    cardTheme: CardTheme(
      color: Color(0x40FFFFFF),
    ),
    iconTheme: IconThemeData(
      color: Colors.white54,
    ),
    textTheme: TextTheme(
      headline6: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
          fontFamily: 'Rubik',
          fontSize: 14),
      headline3: TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.bold,
        fontFamily: 'Rubik',
        fontSize: 30,
      ),
      headline5: TextStyle(
        color: Color(0xFF3B424E),
        letterSpacing: 1.5,
        fontSize: 18.0,
        fontWeight: FontWeight.bold,
        fontFamily: 'Rubik',
      ),
      subtitle2: TextStyle(
        color: Colors.white70,
        fontSize: 12.0,
        fontFamily: 'Rubik',
      ),
      subtitle1: TextStyle(
        color: Colors.white70,
        fontSize: 18.0,
        fontFamily: 'Rubik',
      ),
    ),
  );
}
