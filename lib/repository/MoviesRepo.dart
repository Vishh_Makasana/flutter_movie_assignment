import 'package:movies_app/model/Movies.dart';
import 'package:movies_app/networking/MoviesApi.dart';
import 'package:movies_app/utility/constants.dart';

class MoviesRepo {
  static Future<List<Movies>> getRepositoriesWithSearchQuery(
      String query) async {
    final uri = Uri.https(baseURL, '', {'s': query, 'apikey': apiKey});

    final jsonResponse = await MoviesApi.getJson(uri);

    //print('$jsonResponse');

    if (jsonResponse == null) {
      return null;
    }

    if (jsonResponse['Error'] != null) {
      return null;
    }

    // print('Error null');

    if (jsonResponse['Search'] == null) {
      return List();
    }

    return Movies.mapJSONStringToList(jsonResponse['Search']);
  }

  static Future<List<Movies>> getRepositoriesWithTitleYear(
      String title, String year) async {
    final uri =
        Uri.https(baseURL, '', {'t': title, 'y': year, 'apikey': 'bf889aec'});

    final jsonResponse = await MoviesApi.getJson(uri);

    // print('$jsonResponse');

    if (jsonResponse == null) {
      return null;
    }

    if (jsonResponse['Error'] != null) {
      return null;
    }

    //print('No Error');

    if (jsonResponse['items'] == null) {
      return List();
    }

    return Movies.mapJSONStringToList(jsonResponse['items']);
  }
}
