import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:movies_app/networking/MoviesApi.dart';
import 'package:movies_app/repository/MoviesRepo.dart';
import 'package:provider/provider.dart';

import '../provider/AppStateNotifier.dart';
import 'MovieSearchListScreen.dart';
import '../utility/constants.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _isLogging = false;
  String buttonText = "LOGIN";
  final _textEmail = TextEditingController();
  final _textPassword = TextEditingController();
  bool _validateEmail = false;
  bool _validatePassword = false;
  String passWordError = "Password Can\'t Be Empty";

  Widget _buildEmailTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Username',
          style: Theme.of(context).textTheme.headline6,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: Hero(
            tag: "search_query",
            child: TextField(
              keyboardType: TextInputType.text,
              onChanged: (text) {
                setState(() {
                  text.trim().length > 0
                      ? _validateEmail = false
                      : _validateEmail = true;
                });
              },
              controller: _textEmail,
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Rubik',
              ),
              textCapitalization: TextCapitalization.sentences,
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.only(top: 14.0),
                prefixIcon: Icon(
                  Icons.person,
                  color: Colors.white,
                ),
                hintText: 'Username',
                hintStyle: kHintTextStyle,
              ),
            ),
          ),
        ),
        _validateEmail
            ? Container(
                alignment: Alignment.centerRight,
                padding: EdgeInsets.only(top: 3.0),
                child: Text(
                  _validateEmail ? 'Username Can\'t Be Empty' : '',
                  style: Theme.of(context).textTheme.subtitle2,
                ),
              )
            : Container(),
      ],
    );
  }

  Widget _buildPasswordTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Password',
          style: Theme.of(context).textTheme.headline6,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextField(
            keyboardType: TextInputType.number,
            onChanged: (text) {
              setState(() {
                text.trim().length > 0
                    ? _validatePassword = false
                    : _validatePassword = true;
              });
            },
            obscureText: true,
            controller: _textPassword,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'Rubik',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.lock,
                color: Colors.white,
              ),
              hintText: 'Password',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
        _validatePassword
            ? Container(
                alignment: Alignment.centerRight,
                padding: EdgeInsets.only(top: 3.0),
                child: Text(
                  _validatePassword ? passWordError : '',
                  style: Theme.of(context).textTheme.subtitle2,
                ),
              )
            : Container(),
      ],
    );
  }

  Widget _buildLoginBtn() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 25.0),
      width: double.infinity,
      child: RaisedButton(
        elevation: 5.0,
        onPressed: () => {
          if (_textEmail.text.isEmpty || _textPassword.text.isEmpty)
            {
              setState(() {
                _textEmail.text.isEmpty
                    ? _validateEmail = true
                    : _validateEmail = false;
                _textPassword.text.isEmpty
                    ? _validatePassword = true
                    : _validatePassword = false;
              }),
            }
          else if (_textPassword.text.trim().length != 4)
            {
              setState(() {
                _textEmail.text.isEmpty
                    ? _validateEmail = true
                    : _validateEmail = false;
                passWordError = 'Password Can\'t Be More or Less 4 Digits';
                _validatePassword = true;
              }),
            }
          else if (!_isLogging)
            {
              setState(() {
                _validateEmail = false;
                _validatePassword = false;
              }),
              performLogin(_textEmail.text, _textPassword.text)
            }
        },
        padding: EdgeInsets.all(15.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        color: Theme.of(context).colorScheme.secondary,
        child: Text(
          buttonText,
          style: Theme.of(context).textTheme.headline5,
        ),
      ),
    );
  }

  Widget _buildThemeSwitch() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          'Light',
          style: Theme.of(context).textTheme.subtitle1,
        ),
        Switch(
          value: Provider.of<AppStateNotifier>(context).isDarkMode,
          onChanged: (boolVal) {
            Provider.of<AppStateNotifier>(context, listen: false)
                .updateTheme(boolVal);
            print(boolVal);
          },
        ),
        Text(
          'Dark',
          style: Theme.of(context).textTheme.subtitle1,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 40.0,
                    vertical: 40.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: 30.0,
                      ),
                      Text(
                        'Sign In',
                        style: Theme.of(context).textTheme.headline3,
                      ),
                      SizedBox(height: 30.0),
                      _buildEmailTF(),
                      SizedBox(
                        height: 30.0,
                      ),
                      _buildPasswordTF(),
                      SizedBox(
                        height: 30.0,
                      ),
                      _buildLoginBtn(),
                      SizedBox(
                        height: 10.0,
                      ),
                      _buildThemeSwitch()
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void performLogin(String title, String year) async {
    setState(() {
      _isLogging = true;
      buttonText = "Please Wait...";
      FocusScope.of(context).requestFocus(FocusNode());
    });

    final result = await MoviesRepo.getRepositoriesWithTitleYear(title, year);
    if (this.mounted) {
      print(result);
      setState(() {
        if (result != null) {
          // goto home search screen
          buttonText = "LOGIN";
          _isLogging = false;
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => MovieSearchList(
                  searchQuery: title,
                ),
              ));
        } else {
          buttonText = "LOGIN";
          _isLogging = false;

          Fluttertoast.showToast(
              msg: "No User Found",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 2,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
        }
      });
    }
  }
}
