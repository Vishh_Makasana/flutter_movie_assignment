import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:movies_app/model/Movies.dart';
import 'package:movies_app/repository/MoviesRepo.dart';
import 'package:movies_app/ui/MovieItem.dart';

class MovieSearchList extends StatefulWidget {
  final String searchQuery;

  MovieSearchList({Key key, @required this.searchQuery}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MovieSearchState(searchQuery);
}

class _MovieSearchState extends State<MovieSearchList> {
  String searchQuery;
  final key = GlobalKey<ScaffoldState>();
  TextEditingController _searchQuery;
  bool _isSearching = false;
  String _error;
  List<Movies> _results = List();
  Timer debounceTimer;

  _MovieSearchState(this.searchQuery) {
    _searchQuery = TextEditingController(text: searchQuery);
    _searchQuery.addListener(() {
      if (debounceTimer != null) {
        //   print('cancel timer');
        debounceTimer.cancel();
      }
      print(_searchQuery.text);
      debounceTimer = Timer(Duration(milliseconds: 800), () {
        if (this.mounted) {
          performSearch(_searchQuery.text);
        }
      });
    });
  }

  void performSearch(String query) async {
    // print('performSearch: ${_searchQuery.text}');
    if (query.isEmpty) {
      setState(() {
        _isSearching = false;
        _error = null;
        _results = List();
      });
      return;
    }

    setState(() {
      _isSearching = true;
      _error = null;
      _results = List();
    });

    final repos = await MoviesRepo.getRepositoriesWithSearchQuery(query);
    if (this._searchQuery.text == query && this.mounted) {
      setState(() {
        _isSearching = false;
        if (repos != null) {
          _results = repos;
        } else {
          _error = 'No movies found';
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: key,
        appBar: AppBar(
          centerTitle: true,
          iconTheme: Theme.of(context).appBarTheme.iconTheme,
          backgroundColor: Theme.of(context).appBarTheme.color,
          title: TextField(
            autofocus: true,
            controller: _searchQuery,
            style: Theme.of(context).textTheme.subtitle1,
            textCapitalization: TextCapitalization.words,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: "Search movies...",
              hintStyle: TextStyle(color: Theme.of(context).hintColor),
            ),
          ),
        ),
        body: buildBody(context));
  }

  Widget buildBody(BuildContext context) {
    if (_isSearching) {
      return CenterTitle('Searching OMDB...');
    } else if (_error != null) {
      return CenterTitle(_error);
    } else if (_searchQuery.text.isEmpty) {
      return CenterTitle('Begin Search by typing on search bar');
    } else {
      return StaggeredGridView.countBuilder(
        crossAxisCount: 2,
        itemCount: _results.length,
        itemBuilder: (BuildContext context, int index) =>
            MovieItem(_results[index]),
        staggeredTileBuilder: (int index) => index.isEven
            ? StaggeredTile.count(1, 1)
            : StaggeredTile.count(1, 2),
      );
    }
  }
}

class CenterTitle extends StatelessWidget {
  final String title;

  CenterTitle(this.title);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
        alignment: Alignment.center,
        child: Text(
          title,
          style: Theme.of(context).textTheme.subtitle1,
          textAlign: TextAlign.center,
        ));
  }
}
