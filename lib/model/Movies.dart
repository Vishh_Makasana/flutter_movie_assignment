class Movies {
  final String id;
  final String title;
  final String poster;
  final String year;

  Movies(this.id, this.title, this.poster, this.year);

  static List<Movies> mapJSONStringToList(List<dynamic> jsonList) {
    return jsonList
        .map((r) => Movies(r['imdbID'], r['Title'], r['Poster'],
        r['Year']))
        .toList();
  }
}